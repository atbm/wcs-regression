<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Trouvez un magasin</name>
   <tag></tag>
   <elementGuidId>cc35dcf2-9066-45ee-8e5a-d017b301f6ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#contentRecommendationWidget_10_-2000_3074457345618294037 > div > div.HOPWrapper > div > div > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='contentRecommendationWidget_10_-2000_3074457345618294037']/div/div/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Trouvez un magasin</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;contentRecommendationWidget_10_-2000_3074457345618294037&quot;)/div[@class=&quot;left_espot AnalyticsPromo deledeBorderMobile&quot;]/div[@class=&quot;HOPWrapper&quot;]/div[@class=&quot;buttoncontainer&quot;]/div[@class=&quot;HOPButton&quot;]/span[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='contentRecommendationWidget_10_-2000_3074457345618294037']/div/div/div/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('et hop ! on l', &quot;'&quot;, 'essaie')])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='comparer'])[20]/following::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='POUR VOUS FACILITER LA VIE'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Vous pouvez dormir sur vos deux oreilles...'])[1]/preceding::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[18]/div/div/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
