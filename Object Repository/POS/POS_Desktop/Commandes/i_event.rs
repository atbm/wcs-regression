<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_event</name>
   <tag></tag>
   <elementGuidId>5ba76aaa-1ed3-440b-80ff-520d3bd08e0f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__BVID__180___BV_modal_body_']/div/div/div[2]/form/div/form/div/div/div/div/div/div/button/i</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//i[@class = 'material-icons' and (text() = 'event' or . = 'event')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-icons</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>event</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__BVID__180___BV_modal_body_&quot;)/div[1]/div[@class=&quot;mb-3&quot;]/div[@class=&quot;px-3 px-md-4 mt-3 mt-md-4&quot;]/form[1]/div[1]/form[1]/div[@class=&quot;row border-bottom&quot;]/div[@class=&quot;col&quot;]/div[@class=&quot;mb-3&quot;]/div[@class=&quot;form-group m-0&quot;]/div[@class=&quot;input-group&quot;]/div[@class=&quot;input-group-prepend&quot;]/button[@class=&quot;btn btn-primary&quot;]/i[@class=&quot;material-icons&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__BVID__180___BV_modal_body_']/div/div/div[2]/form/div/form/div/div/div/div/div/div/button/i</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date'])[1]/following::i[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Disponibilités de livraison'])[1]/following::i[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='hourglass_empty'])[1]/preceding::i[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='error_outline'])[5]/preceding::i[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/button/i</value>
   </webElementXpaths>
</WebElementEntity>
