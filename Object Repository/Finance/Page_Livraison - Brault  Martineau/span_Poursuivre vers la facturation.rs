<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Poursuivre vers la facturation</name>
   <tag></tag>
   <elementGuidId>b4b163b8-30e3-44dc-8a0d-80f109e33191</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#WC_Registered_Shipping_Info_Continue_1 > span.button_text</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;WC_Unregistered_Shipping_Info_Continue_2&quot;)/span[@class=&quot;button_text&quot;][count(. | //*[(text() = 'Poursuivre vers la facturation*' or . = 'Poursuivre vers la facturation*')]) = count(//*[(text() = 'Poursuivre vers la facturation*' or . = 'Poursuivre vers la facturation*')])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='WC_Unregistered_Shipping_Info_Continue_2']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button_text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Poursuivre vers la facturation*</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;WC_Unregistered_Shipping_Info_Continue_2&quot;)/span[@class=&quot;button_text&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='WC_Unregistered_Shipping_Info_Continue_2']/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Vous devez compléter chacun des champs avant de poursuivre.'])[1]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Chez Brault &amp; Martineau, nous réduisons le temps d', &quot;'&quot;, 'attente!')])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Termes et conditions'])[1]/preceding::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='la Politique de confidentialité.'])[1]/preceding::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/a/span</value>
   </webElementXpaths>
</WebElementEntity>
