
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String


def static "var.addNewMail.randomMail"() {
    (new var.addNewMail()).randomMail()
}

def static "var.addNewMailEco.randomMail"() {
    (new var.addNewMailEco()).randomMail()
}

def static "connectionDB_test.test.connectDB"(
    	String url	
     , 	String dbname	
     , 	String port	
     , 	String username	
     , 	String password	) {
    (new connectionDB_test.test()).connectDB(
        	url
         , 	dbname
         , 	port
         , 	username
         , 	password)
}

def static "connectionDB_test.test.executeQuery"(
    	String queryString	) {
    (new connectionDB_test.test()).executeQuery(
        	queryString)
}

def static "connectionDB_test.test.closeDatabaseConnection"() {
    (new connectionDB_test.test()).closeDatabaseConnection()
}

def static "connectionDB_test.test.execute"(
    	String queryString	) {
    (new connectionDB_test.test()).execute(
        	queryString)
}
