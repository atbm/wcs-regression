import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/brault_Desk')

suiteProperties.put('name', 'brault_Desk')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\t.agboton\\Documents\\wcs-regression\\Reports\\brault_Desk\\20190724_163530\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/brault_Desk', suiteProperties, [new TestCaseBinding('Test Cases/Brault/Bault_DESKTOP/Brault_creation_compte', 'Test Cases/Brault/Bault_DESKTOP/Brault_creation_compte',  null), new TestCaseBinding('Test Cases/Brault/Bault_DESKTOP/Brault_InfosLettre', 'Test Cases/Brault/Bault_DESKTOP/Brault_InfosLettre',  null), new TestCaseBinding('Test Cases/Brault/Bault_DESKTOP/Brault_Menu_Bypass_Departement', 'Test Cases/Brault/Bault_DESKTOP/Brault_Menu_Bypass_Departement',  null), new TestCaseBinding('Test Cases/Brault/Bault_DESKTOP/Brault_ValidationGlobalMenu', 'Test Cases/Brault/Bault_DESKTOP/Brault_ValidationGlobalMenu',  null), new TestCaseBinding('Test Cases/Brault/Bault_DESKTOP/Commande/Brault_Commande', 'Test Cases/Brault/Bault_DESKTOP/Commande/Brault_Commande',  null), new TestCaseBinding('Test Cases/Brault/Bault_DESKTOP/homePage/Brault_Footer', 'Test Cases/Brault/Bault_DESKTOP/homePage/Brault_Footer',  null), new TestCaseBinding('Test Cases/Brault/Bault_DESKTOP/homePage/Brault_Home_Menu', 'Test Cases/Brault/Bault_DESKTOP/homePage/Brault_Home_Menu',  null), new TestCaseBinding('Test Cases/Brault/Bault_DESKTOP/homePage/Brault_Plan BM', 'Test Cases/Brault/Bault_DESKTOP/homePage/Brault_Plan BM',  null), new TestCaseBinding('Test Cases/Brault/Bault_DESKTOP/homePage/Brault_Social_Media', 'Test Cases/Brault/Bault_DESKTOP/homePage/Brault_Social_Media',  null), new TestCaseBinding('Test Cases/Brault/Bault_DESKTOP/Hop', 'Test Cases/Brault/Bault_DESKTOP/Hop',  null)])
