package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object urlBM
     
    /**
     * <p></p>
     */
    public static Object urlECO
     
    /**
     * <p></p>
     */
    public static Object urlPOS
     
    /**
     * <p></p>
     */
    public static Object User
     
    /**
     * <p></p>
     */
    public static Object PassWord
     
    /**
     * <p></p>
     */
    public static Object UrlBrault
     
    /**
     * <p></p>
     */
    public static Object UrlEco
     
    /**
     * <p></p>
     */
    public static Object UrlPOS
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            urlBM = selectedVariables['urlBM']
            urlECO = selectedVariables['urlECO']
            urlPOS = selectedVariables['urlPOS']
            User = selectedVariables['User']
            PassWord = selectedVariables['PassWord']
            UrlBrault = selectedVariables['UrlBrault']
            UrlEco = selectedVariables['UrlEco']
            UrlPOS = selectedVariables['UrlPOS']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
