<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>POS_Desk</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0935dd33-adb1-4f6e-9b6d-0345f72ec54e</testSuiteGuid>
   <testCaseLink>
      <guid>38664b59-4608-48ce-b603-5dc34ed9fd1c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/POS/POS_Desktop/Commandes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab8e9708-c0ed-42b0-819e-2eb414fafa4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/POS/POS_Desktop/ConnexionDeconnexion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bae059bc-93e8-4f2d-991f-6cf59c7891b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/POS/POS_Desktop/FausseConnexion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88abcb1a-94f4-40aa-8db0-a091d8527e4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/POS/POS_Desktop/VerifierMenu</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
