import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.doc, true)

Mobile.pressBack()

Mobile.tap(findTestObject('Economax/Economax_Mobile/Eco_Menu/android.widget.TextView1 - EconoMax'), 0)

WebUI.scrollToElement(findTestObject('Economax/Economax_Mobile/Eco_Menu/android.view.View117 - ... Meubles'), 0)

Mobile.tap(findTestObject('Economax/Economax_Mobile/Eco_Menu/android.view.View117 - ... Meubles'), 0)

Mobile.tap(findTestObject('Eco_Mobile_1/android.view.View7521211564 - Salon'), 0)

Mobile.tap(findTestObject('Economax/Economax_Mobile/Eco_Menu/android.view.View76 - Sofas sectionnels'), 0)

