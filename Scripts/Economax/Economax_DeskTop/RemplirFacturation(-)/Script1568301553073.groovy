import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

title = WebUI.getWindowTitle()

WebUI.delay(5)

WebUI.verifyMatch(title, 'Facturation - EconoMax', false)

WebUI.delay(5)

WebUI.sendKeys(findTestObject('Economax/Economax_Desktop/RemplirFacturation/input_Numro de carte_card_number'), '4530910000012345')

WebUI.delay(5)

WebUI.sendKeys(findTestObject('Economax/Economax_Desktop/RemplirFacturation/input_concat(Qu  est-ce qu  un code de scurit)_card_cvv'),
	'235')

WebUI.delay(5)

WebUI.click(findTestObject('Economax/Economax_Desktop/RemplirFacturation/table_05'))

WebUI.delay(5)

WebUI.click(findTestObject('Economax/Economax_Desktop/RemplirFacturation/td_09'))

WebUI.delay(5)

WebUI.click(findTestObject('Economax/Economax_Desktop/RemplirFacturation/div_Continuer vers la validation'))

WebUI.delay(5)

title = WebUI.getWindowTitle()

WebUI.delay(5)

WebUI.verifyMatch(title, 'Validation de commande - EconoMax', false)

WebUI.delay(5)

WebUI.closeBrowser()

