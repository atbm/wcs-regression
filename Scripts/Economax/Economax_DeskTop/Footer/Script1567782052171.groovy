import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.UrlEco)

WebUI.deleteAllCookies()

WebUI.delay(3)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_Financement'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Financement | EconoMax', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_Livraison'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Livraison | EconoMax', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_Politique de retour'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Politique de retour | EconoMax', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_Garantie du meilleur prix'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Garantie meilleur prix | EconoMax', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_Contactez-nous'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Contactez-nous | EconoMax', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_ propos'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'À propos | EconoMax', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_Me connecter  Minscrire'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Se connecter', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_Trouver un magasin'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Localisateur de magasins', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_Carrires'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.switchToWindowIndex(0)

WebUI.delay(3)

/* WebUI.verifyMatch(title, 'Travailler en magasin', false)

WebUI.delay(3)

WebUI.switchToWindowIndex(0)

WebUI.delay(3) */

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_Circulaire'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Circulaire | EconoMax', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_English'))

title = WebUI.getWindowTitle()

WebUI.delay(3) 

WebUI.verifyMatch(title, 'EconoMax | Deals on Furniture, Mattresses, Appliances & More', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_Meubles'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Meubles | EconoMax', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_lectromnagers'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Électroménagers | EconoMax', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_Lits et Matelas'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Lits et Matelas | EconoMax', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_lectronique'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Électronique | EconoMax', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/Footer/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_Dcoration'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Décoration | EconoMax', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.closeBrowser()


