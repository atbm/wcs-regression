import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.UrlEco)

WebUI.deleteAllCookies()

WebUI.maximizeWindow()

WebUI.delay(3)

WebUI.click(findTestObject('Reseaux_Eco/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_concat(Je m  abonne)_footerFacebookLink'))

WebUI.delay(3)

title = WebUI.getWindowTitle()

WebUI.delay(3)
/*
WebUI.verifyMatch(title, 'EconoMax - Accueil | Facebook', false)

WebUI.delay(3)
*/
WebUI.switchToWindowIndex(0)

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'EconoMax | Meubles, Électronique, Électroménagers & Décoration', false)

WebUI.delay(3)

WebUI.click(findTestObject('Reseaux_Eco/Page_EconoMax  Meubles lectronique lectromnagers  Dcoration/a_concat(Je m  abonne)_instagramLink'))

WebUI.delay(3)

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, '@economaxmeubles (@economaxmeubles) • Instagram photos and videos', false)

WebUI.delay(3)

WebUI.switchToWindowIndex(0)

WebUI.delay(3)

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'EconoMax | Meubles, Électronique, Électroménagers & Décoration', false)

WebUI.delay(3)

WebUI.closeBrowser()

