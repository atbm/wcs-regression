import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.UrlEco)

WebUI.deleteAllCookies()

WebUI.delay(5)

WebUI.click(findTestObject('Economax/Economax_Desktop/InfoLettre/div_Je mabonne (1)'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Inscription à l\'infolettre', false)

WebUI.delay(3)

WebUI.sendKeys(findTestObject('Economax/Economax_Desktop/InfoLettre/input__firstName'), 'Sarah')

WebUI.delay(3)

WebUI.sendKeys(findTestObject('Economax/Economax_Desktop/InfoLettre/input__lastName'), 'DeMontagne')

WebUI.delay(3)

WebUI.sendKeys(findTestObject('Economax/Economax_Desktop/InfoLettre/input__email'), 'Sarahdemontagne@demontagne.com')

WebUI.delay(3)

WebUI.sendKeys(findTestObject('Economax/Economax_Desktop/InfoLettre/input__emailConfirmation'), 'Sarahdemontagne@demontagne.com')

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/InfoLettre/validation Abonnement'))

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/InfoLettre/div_Confirmer mon abonnement'))

WebUI.closeBrowser()

