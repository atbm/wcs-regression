import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.callTestCase(findTestCase('Economax/Economax_DeskTop/Commande'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.callTestCase(findTestCase('Economax/Economax_DeskTop/Commande'), [:], FailureHandling.STOP_ON_FAILURE)

title = WebUI.getWindowTitle()

WebUI.delay(5)

WebUI.verifyMatch(title, 'Passer à la caisse - EconoMax', false)

WebUI.delay(3)

WebUI.click(findTestObject('Economax/Economax_Desktop/ConnexionCreeretPasserCaisse/span_Crer un compte et passer  la caisse'))

WebUI.delay(3)

WebUI.sendKeys(findTestObject('Economax/Economax_Desktop/ConnexionCreeretPasserCaisse/input__firstName'), 'TestEcono')

WebUI.delay(5)

WebUI.sendKeys(findTestObject('Economax/Economax_Desktop/ConnexionCreeretPasserCaisse/input__lastName'), 'Max')

WebUI.delay(5)

mail = CustomKeywords.'var.addNewMailEco.randomMail'()

WebUI.sendKeys(findTestObject('Economax/Economax_Desktop/ConnexionCreeretPasserCaisse/input__email1'), mail)

WebUI.delay(5)

WebUI.sendKeys(findTestObject('Economax/Economax_Desktop/ConnexionCreeretPasserCaisse/input__phone1'), '5145898912')

WebUI.callTestCase(findTestCase('Brault/Bault_DESKTOP/laPoste(-)'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Economax/Economax_Desktop/ConnexionCreeretPasserCaisse/motDePasse'), 'test123*')

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Economax/Economax_Desktop/ConnexionCreeretPasserCaisse/verifyPassWord'), 'test123*')

WebUI.delay(3)

WebUI.delay(1)

WebUI.click(findTestObject('Economax/Economax_Desktop/ConnexionCreeretPasserCaisse/span_Poursuivre vers la facturation'))

