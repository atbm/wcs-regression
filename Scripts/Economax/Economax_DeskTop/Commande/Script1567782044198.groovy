import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.UrlEco)

WebUI.deleteAllCookies()

WebUI.maximizeWindow()

WebUI.delay(5)

WebUI.mouseOver(findTestObject('Economax/Economax_Desktop/Commande/span_lectromnagers'))

WebUI.delay(5)

WebUI.click(findTestObject('Economax/Economax_Desktop/Commande/span_lectromnagers'))

WebUI.delay(5)

WebUI.mouseOver(findTestObject('Economax/Economax_Desktop/Commande/a_Laveuses et scheuses'))

WebUI.delay(5)

WebUI.click(findTestObject('Economax/Economax_Desktop/Commande/a_Laveuses et scheuses'))

WebUI.delay(5)

title = WebUI.getWindowTitle()

WebUI.delay(5)

WebUI.verifyMatch(title, 'Laveuses et sécheuses | EconoMax', false)

WebUI.delay(5)

WebUI.scrollToPosition(800, 749)

WebUI.delay(5)

WebUI.mouseOver(findTestObject('Economax/Economax_Desktop/Commande/h2_Laveuses'))

WebUI.delay(5)

WebUI.click(findTestObject('Economax/Economax_Desktop/Commande/h2_Laveuses'))

WebUI.delay(5)

title = WebUI.getWindowTitle()

WebUI.delay(5)

WebUI.verifyMatch(title, 'Laveuses | EconoMax', false)

WebUI.delay(5)

WebUI.scrollToPosition(1100, 1200)

WebUI.delay(5)

WebUI.click(findTestObject('Economax/Economax_Desktop/Commande/a_Laveuse  chargement par le haut de 44pi - Whirlpool WTW4855HW'))

WebUI.delay(5)

WebUI.click(findTestObject('Economax/Economax_Desktop/Commande/span_Ajouter au panier'))

WebUI.delay(5)

WebUI.click(findTestObject('Economax/Economax_Desktop/Commande/span_Accder au panier'))

WebUI.delay(5)

title = WebUI.getWindowTitle()

WebUI.delay(5)

WebUI.verifyMatch(title, 'Panier - EconoMax', false)

WebUI.delay(5)

WebUI.verifyElementText(findTestObject('Economax/Economax_Desktop/Commande/div_Passer  la caisse'), 'Passer à la caisse')

WebUI.delay(5)

WebUI.click(findTestObject('Economax/Economax_Desktop/Commande/div_Passer  la caisse'))

WebUI.delay(5)

