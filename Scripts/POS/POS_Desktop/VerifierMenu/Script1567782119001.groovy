import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.docTA)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/h2_Ouvrir une session'), 'OUVRIR UNE SESSION')

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/a_Changer mon mot de passe  lock'), 'Changer mon mot de passe lock')

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/label_Se souvenir de moi'), 'Se souvenir de moi')

WebUI.delay(3)

WebUI.setText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/input_Identifiant'), GlobalVariable.User)

WebUI.delay(3)

WebUI.setText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/input_Mot de passe'), GlobalVariable.PassWord)

WebUI.delay(3)

WebUI.click(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/label_Se souvenir de moi'))

WebUI.delay(3)

WebUI.click(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/button_Soumettre'))

WebUI.delay(3)

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'POS - QA', false)

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/Commandes/h1_Bienvenue'), 'Bienvenue')

WebUI.delay(3)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/i_shopping_cart'))

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/Commandes/h1_Panier'), 'Panier')

WebUI.delay(3)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA (1)/i_weekend'))

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA (1)/h1_Recherche de produits'), 'Recherche de produits')

/*WebUI.delay(3)

WebUI.click(findTestObject('POS _desktop_connexion/Validation_menu/Page_POS - QA/Page_POS - QA/i_local_shipping'))

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('POS _desktop_connexion/Validation_menu/Page_POS - QA/h1_Disponibilits de livraison'), 
    'Disponibil�s de livraison')
*/
WebUI.delay(3)

WebUI.click(findTestObject('POS/POS_Desktop/VerifierMenu/i_description'))

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/VerifierMenu/h1_Recherche de commandes'), 
    'Recherche de commandes')

WebUI.delay(3)

WebUI.click(findTestObject('POS/POS_Desktop/VerifierMenu/i_person (1)'))

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/VerifierMenu/h1_Rechercher un client'), 
    'Rechercher un client')

WebUI.delay(3)

WebUI.click(findTestObject('POS/POS_Desktop/VerifierMenu/i_screen_share'))

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/VerifierMenu/h1_Rechercher un site fournisseur'), 
    'Rechercher un site fournisseur')

WebUI.delay(3)

WebUI.click(findTestObject('POS/POS_Desktop/VerifierMenu/i_chevron_right_fa fa-bullhorn (1)'))

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/VerifierMenu/h1_Rechercher une publicit'), 
    'Rechercher une publicit�')

WebUI.closeBrowser()

