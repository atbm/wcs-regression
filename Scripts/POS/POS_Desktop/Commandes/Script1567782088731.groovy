import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.UrlPOS)

WebUI.deleteAllCookies()

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/h2_Ouvrir une session'), 'OUVRIR UNE SESSION')

WebUI.delay(5)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/a_Changer mon mot de passe  lock'), 'Changer mon mot de passe lock')

WebUI.delay(5)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/label_Se souvenir de moi'), 'Se souvenir de moi')

WebUI.delay(5)

WebUI.setText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/input_Identifiant'), GlobalVariable.User)

WebUI.delay(5)

WebUI.setText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/input_Mot de passe'), GlobalVariable.PassWord)

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/label_Se souvenir de moi'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/button_Soumettre'))

WebUI.delay(5)

title = WebUI.getWindowTitle()

WebUI.delay(5)

WebUI.verifyMatch(title, 'POS - QA', false)

WebUI.delay(5)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/Commandes/h1_Bienvenue'), 'Bienvenue')

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/i_shopping_cart'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA/span_Associer un client'))

WebUI.delay(5)

WebUI.sendKeys(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA/input_Nom'), 'PECQUET')

WebUI.delay(5)

WebUI.sendKeys(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA/input_Prnom'), 'Pierre')

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA/i_search'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA (1)/i_shopping_cart'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA/button_Confirmer'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA (1)/button_Ok'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA (1)/h3_Ajouter un produit zoom_in'))

WebUI.delay(5)

WebUI.sendKeys(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA (1)/input_No produit'), '00381383')

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA (1)/button_shopping_cart (1)'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA (1)/div_Livraison'))

/*WebUI.delay(5)

WebUI.selectOptionByIndex(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA (1)/select_TypeRgulierDmonstrateur'), 1, FailureHandling.STOP_ON_FAILURE)
*/
WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/button_Choisir une date (1)'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/i_event'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/span_'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/span_19'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/button_Confirmer'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/button_Ajouter au panier'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/button_Conclure la vente'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/Commandes/button_Mode de paiement'))

WebUI.delay(5)

WebUI.selectOptionByIndex(findTestObject('POS/POS_Desktop/Commandes/Page_POS - QA (1)/Paiement'), 2, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.click(findTestObject('end test/Page_POS - QA/button_Confirmer'))

WebUI.delay(5)

WebUI.click(findTestObject('end test/Page_POS - QA/button_Paiement  la caisse'))

WebUI.delay(5)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/Commandes/div_La commande a t cre avec succs'), 
    'La commande a �t� cr��e avec succ�s! Vous pouvez cliquer sur le bouton ci-dessous pour acc�der � la commande directement.')

WebUI.closeBrowser()

