import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.urlPOS)

WebUI.verifyElementText(findTestObject('Desktop_ Test_connexion/Page_POS - QA/h2_Ouvrir une session'), 'OUVRIR UNE SESSION')

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('Desktop_ Test_connexion/Page_POS - QA/a_Changer mon mot de passe  lock'), 'Changer mon mot de passe lock')

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('Desktop_ Test_connexion/Page_POS - QA/label_Se souvenir de moi'), 'Se souvenir de moi')

WebUI.delay(3)

WebUI.setText(findTestObject('Desktop_ Test_connexion/Page_POS - QA/input_Identifiant'), '12559698')

WebUI.delay(3)

WebUI.setText(findTestObject('Desktop_ Test_connexion/Page_POS - QA/input_Mot de passe'), '1259869')

WebUI.delay(3)

WebUI.click(findTestObject('Desktop_ Test_connexion/Page_POS - QA/label_Se souvenir de moi'))

WebUI.delay(3)

WebUI.click(findTestObject('Desktop_ Test_connexion/Page_POS - QA/button_Soumettre'))

//WebUI.delay(3)
//WebUI.verifyElementText(findTestObject('Desktop_ Test_connexion/Page_POS - QA/h1_Erreur'), 'Erreur')
WebUI.delay(3)

WebUI.verifyElementText(findTestObject('Desktop_ Test_connexion/Page_POS - QA/div_La combinaison usagermot de passe que vous avez entr nest pas valide. SVP saisir votre code dusager et mot de passe  nouveau.'), 
    'La combinaison usager/mot de passe que vous avez entré n\'est pas valide. SVP saisir votre code d\'usager et mot de passe à nouveau.')

WebUI.delay(3)

WebUI.click(findTestObject('Desktop_ Test_connexion/Page_POS - QA/button_Ok'))

WebUI.delay(3)

WebUI.closeBrowser()

