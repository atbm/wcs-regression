import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.docTA)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/h2_Ouvrir une session'), 'OUVRIR UNE SESSION')

WebUI.delay(5)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/a_Changer mon mot de passe  lock'), 'Changer mon mot de passe lock')

WebUI.delay(5)

WebUI.verifyElementText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/label_Se souvenir de moi'), 'Se souvenir de moi')

WebUI.delay(5)

WebUI.setText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/input_Identifiant'), GlobalVariable.User)

WebUI.delay(5)

WebUI.setText(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/input_Mot de passe'), GlobalVariable.PassWord)

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/label_Se souvenir de moi'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/FausseConnexion/Page_POS - QA/button_Soumettre'))

WebUI.delay(5)

title = WebUI.getWindowTitle()

WebUI.delay(5)

WebUI.verifyMatch(title, 'POS - QA', false)

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/ConnexionDeconnexion/span_QA6901BM10772'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/ConnexionDeconnexion/a_Dconnexion'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/ConnexionDeconnexion/button_Confirmer'))

WebUI.delay(5)

WebUI.click(findTestObject('POS/POS_Desktop/ConnexionDeconnexion/button_Ok'))

WebUI.delay(5)

WebUI.closeBrowser()

