import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\t.agboton\\Documents\\wcs-regression\\androidapp\\APIDemos.apk', true)

Mobile.pressBack()

WebUI.delay(3)

Mobile.pressBack()

WebUI.delay(3)

Mobile.tap(findTestObject('Brault/Brault_Mobile/testLaPoste/Brault  Martineau'), 0)

WebUI.delay(3)

Mobile.tap(findTestObject('Brault/Brault_Mobile/testMobile/android.view.MenuItem0'), 0)

WebUI.delay(3)

Mobile.tap(findTestObject('Brault/Brault_Mobile/testMobile/Crer un compte'), 0)

WebUI.delay(3)

Mobile.sendKeys(findTestObject('Brault/Brault_Mobile/testMobile/android.widget.EditText1'), 'testwebbm1@gmail.com')

WebUI.delay(3)

Mobile.sendKeys(findTestObject('Brault/Brault_Mobile/testMobile/android.widget.EditText2'), 'test123*')

WebUI.delay(3)

Mobile.sendKeys(findTestObject('Brault/Brault_Mobile/testMobile/android.widget.EditText3'), 'test123*')

WebUI.delay(3)

Mobile.sendKeys(findTestObject('Brault/Brault_Mobile/testMobile/android.widget.EditText4'), 'testlaPOSTE')

WebUI.delay(3)

Mobile.sendKeys(findTestObject('Brault/Brault_Mobile/testMobile/android.widget.EditText5'), 'testlaPOSTE')

WebUI.delay(3)

Mobile.tap(findTestObject('Brault/Brault_Mobile/testMobile/android.widget.Button2 - Soumettre'), 0)

WebUI.delay(3)

Mobile.closeApplication()

