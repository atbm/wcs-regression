import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('C:\\Users\\t.agboton\\Katalon Studio\\Regression_Entiere\\androidapp\\APIDemos.apk', true)

Mobile.pressBack()

Mobile.tap(findTestObject('Brault/Brault_Mobile/brault_connexion/android.view.View10'), 0)

WebUI.delay(5)

Mobile.setText(findTestObject('Brault/Brault_Mobile/brault_connexion/android.widget.EditText0'), 'test01', 0)

WebUI.delay(5)

Mobile.setText(findTestObject('Brault/Brault_Mobile/brault_connexion/android.widget.EditText1 (1)'), 'bonjour01', 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/brault_connexion/android.widget.Button0 - Soumettre'), 1)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/Validation_Menu/android.widget.Button0 - chevron_right'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/Validation_Menu/android.view.View19 - shopping_cart'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/Validation_Menu/android.view.View21 - weekend'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/Validation_Menu/android.view.View23 - local_shipping'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/Validation_Menu/android.view.View25 - description'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/Validation_Menu/android.view.View27 - person'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/Validation_Menu/android.view.View29 - screen_share'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/Validation_Menu/android.view.View31 - '), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/Validation_Menu/android.widget.Button0 - chevron_right'), 0)

Mobile.closeApplication()

