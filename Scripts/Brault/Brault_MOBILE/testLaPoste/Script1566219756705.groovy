import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.detroitlabs.katalonmobileutil.testobject.Finder as Finder
import com.detroitlabs.katalonmobileutil.touch.Scroll as Scroll

Mobile.startApplication(GlobalVariable.docPS, true)

Mobile.pressBack()

WebUI.delay(5)

Mobile.pressBack()

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/testLaPoste/Brault  Martineau'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/testLaPoste/android.view.View8'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/testLaPoste/android.view.View34'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/testLaPoste/android.widget.Spinner1 - Tous nos dpartements (1)'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/testLaPoste/deroule - lectromnagers'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/testLaPoste/android.view.MenuItem11 - Rfrigrateurs'), 0)

WebUI.delay(5)

