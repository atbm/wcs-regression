import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Brault/Brault_MOBILE/Brault_Mobile_Android/Brault_connection'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Brault/Brault_Mobile/brault_connexion/android.widget.EditText0'), GlobalVariable.User, 0)

WebUI.delay(5)

Mobile.setText(findTestObject('Brault/Brault_Mobile/brault_connexion/android.widget.EditText1 (1)'), GlobalVariable.PassWord, 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/brault_connexion/android.widget.Button0 - Soumettre'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/brault_connexion/android.view.View6'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/brault_connexion/android.view.MenuItem1 - Dconnexion'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/brault_connexion/android.widget.Button1 - Confirmer'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault/Brault_Mobile/brault_connexion/android.widget.Button1 - Ok'), 0)

Mobile.closeApplication()

