import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.null)

WebUI.deleteAllCookies()

WebUI.maximizeWindow()

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/PlanBM/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/span_Livraison gratuite'))

WebUI.delay(3)

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Livraison gratuite | Brault & Martineau', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/PlanBM/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/span_Plan de versements sans intrt'))

WebUI.delay(3)

WebUI.delay(3)

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Plan de versements sans intérêt | Brault & Martineau', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/PlanBM/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/span_Ramassage gratuit'))

WebUI.delay(3)

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Ramassage gratuit | Brault & Martineau', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/PlanBM/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/span_Mise de ct gratuite'))

WebUI.delay(3)

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Mise de côté gratuite | Brault & Martineau', false)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/PlanBM/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/span_Matelas 90 jours dessai'))

WebUI.delay(3)

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, '90 jours d\'essai sur les matelas | Brault & Martineau', false)

WebUI.delay(3)

WebUI.back()

WebUI.click(findTestObject('Brault/Brault_Desktop/PlanBM/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/span_Plan de protection'))

WebUI.delay(3)

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Plan de protection disponible | Brault & Martineau', false)

WebUI.delay(3)

WebUI.back()

WebUI.click(findTestObject('Brault/Brault_Desktop/PlanBM/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/span_Service dinstallation'))

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Service d\'installation | Brault & Martineau', false)

WebUI.delay(3)

WebUI.back()

WebUI.closeBrowser()

