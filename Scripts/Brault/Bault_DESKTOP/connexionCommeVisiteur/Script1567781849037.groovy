import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

/*
WebUI.callTestCase(findTestCase('Brault/Bault_DESKTOP/CommandePanier799etPlus'), [:], FailureHandling.STOP_ON_FAILURE)
*/
WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/ConnecionCommeVisiteur/span_Passer  la caisse en tant que visiteur'))

WebUI.delay(3)

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/ConnecionCommeVisiteur/input__firstName'), 'Brault')

WebUI.delay(3)

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/ConnecionCommeVisiteur/input__lastName'), 'Martineau')

WebUI.delay(3)

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/ConnecionCommeVisiteur/input__email1'), 'martineau@martineau.com')

WebUI.delay(3)

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/ConnecionCommeVisiteur/input__phone1'), '5144422569')

WebUI.delay(3)

WebUI.callTestCase(findTestCase('Brault/Bault_DESKTOP/laPoste(-)'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

/*
WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/ConnecionCommeVisiteur/Page_Livraison - Brault  Martineau/input__BNM_Input_Address1'), 
    '8600 Place Marien Montréal-Est, Québec, H1B5W8, Canada')

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/ConnecionCommeVisiteur/Page_Livraison - Brault  Martineau/input__BNM_Input_Address1'), 
    Keys.chord(Keys.DELETE))

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/ConnecionCommeVisiteur/Page_Livraison - Brault  Martineau/input__BNM_Input_Address1'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Brault/Brault_Desktop/ConnecionCommeVisiteur/Page_Livraison - Brault  Martineau/a_Cliquez ici'))

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/ConnecionCommeVisiteur/Page_Livraison - Brault  Martineau/input__address1'), 
    '8600 place marien')

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/ConnecionCommeVisiteur/Page_Livraison - Brault  Martineau/input__BNM_Input_Address1'), 
    '860')

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/ConnecionCommeVisiteur/Page_Livraison - Brault  Martineau/input__city'), 
    'Montréal')

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/ConnecionCommeVisiteur/Page_Livraison - Brault  Martineau/input__zipCode'), 
    'H4N0C5')

WebUI.click(findTestObject('Brault/Brault_Desktop/ConnecionCommeVisiteur/Page_Livraison - Brault  Martineau/div_Sauvegarder ladresse'), 
    FailureHandling.STOP_ON_FAILURE)
    */
