import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.UrlBrault)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/div_Meubles'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/div_lectromnagers'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/div_Dcoration'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/div_Matelas'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/div_lectronique'))

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/div_Meubles'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Salon'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Cuisine  Salle  manger'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Chambre  coucher'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Autres Espaces'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Extrieur'))

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Salon'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Divans'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Divans sectionnels'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Causeuses'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Fauteuils'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Poufs  Bancs'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Tables de salon'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Meubles Audio-Vido  tagres  Curios'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Consoles'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Foyers'))

WebUI.back()

WebUI.click(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/h2_Cuisine  Salle  manger'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/Page_Mobilier de Cuisine  De Salle  Manger  Brault  Martineau/h2_Tables'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/Page_Mobilier de Cuisine  De Salle  Manger  Brault  Martineau/h2_Chaises'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/Page_Mobilier de Cuisine  De Salle  Manger  Brault  Martineau/h2_Tabourets  Bancs'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/Page_Mobilier de Cuisine  De Salle  Manger  Brault  Martineau/h2_Bahuts  Buffets  Vaisseliers'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/Page_Mobilier de Cuisine  De Salle  Manger  Brault  Martineau/h2_Ensembles de cuisine  salle  manger'))

WebUI.delay(3)

WebUI.back(FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/Page_Meubles de Chambre Lits Commodes Armoires etc  Brault  Martineau/h2_Lits'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/Page_Meubles de Chambre Lits Commodes Armoires etc  Brault  Martineau/h2_Tables de chevet'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/Page_Meubles de Chambre Lits Commodes Armoires etc  Brault  Martineau/h2_Commodes  Coffres'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/Page_Meubles de Chambre Lits Commodes Armoires etc  Brault  Martineau/h2_Armoires'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/Page_Meubles de Chambre Lits Commodes Armoires etc  Brault  Martineau/h2_Chiffonniers'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/Page_Meubles de Chambre Lits Commodes Armoires etc  Brault  Martineau/h2_Ensembles de chambre  coucher'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Brault/Brault_Desktop/MenuBypassDepartement/Page_Meubles de Chambre Lits Commodes Armoires etc  Brault  Martineau/h2_Enfants'))

WebUI.delay(3)

