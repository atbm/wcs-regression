import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.UrlBrault)

WebUI.deleteAllCookies()

WebUI.maximizeWindow()

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Brault/Brault_Desktop/CommandePanier799etPlus/CommandePanier799etPlus/span_lectromnagers'))

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/CommandePanier799etPlus/a_Rfrigrateurs'))

WebUI.delay(3)

WebUI.doubleClick(findTestObject('Brault/Brault_Desktop/Financement/Page_Rfrigrateurs avec ou sans conglateur  Brault  Martineau/image'))

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/Financement/Page_Rfrigrateur Conglateur dans le bas Hisense - RB17N6DSE - 00388090  Brault  Martineau/span_Ajouter au panier'))

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/Financement/Page_Rfrigrateur Conglateur dans le bas Hisense - RB17N6DSE - 00388090  Brault  Martineau/span_Accder au panier'))

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/Financement/Page_Panier - Brault  Martineau/div_Passer  la caisse'))

WebUI.delay(3)

WebUI.callTestCase(findTestCase('Brault/Bault_DESKTOP/connexionCommeVisiteur'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('Finance/Page_Livraison - Brault  Martineau/span_Poursuivre vers la facturation'))

WebUI.delay(3)

WebUI.click(findTestObject('Finance/Page_Facturation - Brault  Martineau/input_Payer par carte de crdit_payment_method'))

WebUI.delay(10)

WebUI.doubleClick(findTestObject('Finance/Page_Facturation - Brault  Martineau/label_En cochant'))

WebUI.delay(3)

WebUI.setText(findTestObject('Finance/Page_Facturation - Brault  Martineau/input_Numro de carte_finance_card'), '4530910000012345')

WebUI.delay(3)

WebUI.setText(findTestObject('Finance/Page_Facturation - Brault  Martineau/input_concat(Qu  est-ce qu  un code de scurit)_financing_card_cvv'), 
    '325')

WebUI.delay(3)

WebUI.click(findTestObject('Finance/Page_Facturation - Brault  Martineau/div_Suivant'))

WebUI.delay(3)

WebUI.delay(3)

