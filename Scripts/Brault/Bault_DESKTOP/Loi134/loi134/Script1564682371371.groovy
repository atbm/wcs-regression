import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://bm.qa.ecom.bmtc.ca/')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'lol')

WebUI.setText(findTestObject('null'), 'lol')

WebUI.setText(findTestObject('null'), 'loi134@loi134.loi134')

WebUI.setText(findTestObject('null'), '5142566936')

WebUI.setText(findTestObject('null'), 
    '8600 place marien')

WebUI.click(findTestObject('null'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('null'))

WebUI.verifyElementText(findTestObject('null'), 
    'Payer en 24 versements mensuels égaux en finançant* le montant total avant taxes de votre commande')

WebUI.verifyElementText(findTestObject('null'), 
    'Obtenez une carte de financement Brault & Martineau Desjardins Accord D')

WebUI.verifyElementText(findTestObject('null'), 
    'Faites la demande pour une carte de financement Brault & Martineau Desjardins Accord D et bénéficiez d\'un plan de paiement simplifié et flexible.')

WebUI.verifyElementClickable(findTestObject('null'))

WebUI.verifyElementClickable(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.verifyElementText(findTestObject('null'), 
    'Montant à payer immédiatement par carte de crédit')

WebUI.verifyElementText(findTestObject('null'), 
    'Pour plus d\'informations concernant les modalités de financement, consultez la section financement*.')

WebUI.closeBrowser()

