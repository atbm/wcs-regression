import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.docPS)

WebUI.click(findTestObject('loi134/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/div_Meubles'))

WebUI.click(findTestObject('loi134/Page_Magasin de Meubles En Ligne  Brault  Martineau/h2_Salon'))

WebUI.click(findTestObject('loi134/Page_Meubles de Salon Audio-Vido Tables Sofas Etc  Brault  Martineau/h2_Divans'))

WebUI.click(findTestObject('loi134/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Divan inclinable et motoris en tissu gris fonc avec appuis-tte ajustables'))

WebUI.click(findTestObject('loi134/Page_Divan inclinable et motoris en tissu gris fonc avec appuis-tte ajustables - 00390850  Brault  Martineau/span_Ajouter au panier'))

WebUI.click(findTestObject('loi134/Page_Divan inclinable et motoris en tissu gris fonc avec appuis-tte ajustables - 00390850  Brault  Martineau/span_Accder au panier'))

WebUI.click(findTestObject('loi134/Page_Panier - Brault  Martineau/div_Passer  la caisse'))

WebUI.click(findTestObject('loi134/Page_Passer  la caisse - Brault  Martineau/span_Passer  la caisse en tant que visiteur'))

WebUI.sendKeys(findTestObject('loi134/Page_Livraison - Brault  Martineau/input__firstName'), 'LOI')

WebUI.sendKeys(findTestObject('loi134/Page_Livraison - Brault  Martineau/input__lastName'), 'LOI')

WebUI.sendKeys(findTestObject('loi134/Page_Livraison - Brault  Martineau/input__email1'), 'loi134@loi134.loi134')

WebUI.sendKeys(findTestObject('loi134/Page_Livraison - Brault  Martineau/input__phone1'), '5142566936')

WebUI.sendKeys(findTestObject('Brault Order/Page_Livraison - Brault  Martineau/input__BNM_Input_Address1'), '1100 Rue Notre-Dame O MONTR�AL QC H3C 1K3 CANADA')

WebUI.delay(3)

WebUI.sendKeys(findTestObject('Brault Order/Page_Livraison - Brault  Martineau/input__BNM_Input_Address1'), Keys.chord(Keys.DELETE, 
        Keys.DELETE))

WebUI.delay(3)

WebUI.sendKeys(findTestObject('Brault Order/Page_Livraison - Brault  Martineau/input__BNM_Input_Address1'), Keys.chord(Keys.ENTER, 
        Keys.ENTER))

WebUI.delay(3)

