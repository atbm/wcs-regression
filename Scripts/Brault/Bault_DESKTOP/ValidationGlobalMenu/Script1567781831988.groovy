import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.UrlBrault)

WebUI.click(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/span_Meubles'))

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Salon'), 'SALON')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Divans'), 
    'Divans')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Divans sectionnels'), 
    'Divans sectionnels')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Causeuses'), 
    'Causeuses')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Fauteuils'), 
    'Fauteuils')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Poufs  Bancs'), 
    'Poufs & Bancs')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Tables de salon'), 
    'Tables de salon')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Consoles'), 
    'Consoles')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Meubles Audio-Vido  tagres  Curios'), 
    'Meubles Audio-Vidéo & étagéres & Curios')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Foyers'), 
    'Foyers')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Bureaux'), 
    'Bureaux')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Tables'), 
    'Tables')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Chaises'), 
    'Chaises')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Tabourets  Bancs'), 
    'Tabourets & Bancs')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Bahuts  Buffets  Vaisseliers'), 
    'Bahuts & Buffets & Vaisseliers')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Ensembles de cuisine  salle  manger'), 
    'Ensembles de cuisine & salle a manger')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Mobiliers de Patio'), 
    'Mobiliers de Patio')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Lits'), 'Lits')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Tables de chevet'), 
    'Tables de chevet')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Commodes  Coffres'), 
    'Commodes & Coffres')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Armoires'), 
    'Armoires')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Chiffonniers'), 
    'Chiffonniers')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Coffres  Bancs'), 
    'Coffres & Bancs')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Enfants'), 
    'Enfants')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau/a_Ensembles de chambre  coucher'), 
    'Ensembles de chambre a coucher')

WebUI.click(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (1)/span_lectronique'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (1)/a_Tlviseurs'), 
    'Téléviseurs')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (1)/a_Tlviseurs  Accessoires'), 
    'TÉLÉVISEURS & ACCESSOIRES')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (1)/a_Lecteurs DVD  Blu-Ray'), 
    'Lecteurs DVD & Blu-Ray')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (1)/a_Cinma maison  Barres de son'), 
    'Cinéma maison & Barres de son')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (1)/a_Chaines stro  Stations daccueil  Radios portatives'), 
    'Chaines stéréo & Stations d\'accueil & Radios portatives')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (1)/a_Accessoires'), 
    'Accessoires')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (1)/a_Tlphones sans fil  Combins tlphoniques'), 
    'Téléphones sans fil & Combinés téléphoniques')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (1)/a_Enregistreurs HD PVR  Rcepteurs numriques'), 
    'Enregistreurs HD PVR & Récepteurs numériques')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (1)/a_Ordinateurs portables'), 
    'Ordinateurs portables')

WebUI.click(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/span_lectromnagers'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Rfrigrateurs  Conglateurs'), 
    'RÉFRIGÉRATEURS & CONGÉLATEURS')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Rfrigrateurs'), 
    'Réfrigérateurs')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Conglateurs'), 
    'Congélateurs')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Laveuses  Scheuses'), 
    'LAVEUSES & SÉCHEUSES')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Ensembles de Laveuses  Scheuses'), 
    'Ensembles de Laveuses & Sécheuses')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Laveuses'), 
    'Laveuses')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Scheuses'), 
    'Sécheuses')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Aspirateurs  Accessoires'), 
    'ASPIRATEURS & ACCESSOIRES')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Aspirateurs'), 
    'Aspirateurs')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Cuisson'), 
    'CUISSON')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Barbecues'), 
    'Barbecues')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Cuisinires'), 
    'Cuisiniéres')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Fours encastrables'), 
    'Fours encastrables')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Plaques de cuisson'), 
    'Plaques de cuisson')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Hottes'), 
    'Hottes')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Fours micro-ondes avec hottes'), 
    'Fours micro-ondes avec hottes')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Petits lectromnagers'), 
    'PETITS ÉLECTROMÉNAGERS')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Machines  caf'), 
    'Machines a  café')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Fours  micro-ondes'), 
    'Fours a micro-ondes')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Climatisation  Traitement de lair'), 
    'CLIMATISATION & TRAITEMENT DE L\'AIR')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Dshumidificateurs  Purificateurs'), 
    'Déshumidificateurs & Purificateurs')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Lave-vaisselle  Accessoires'), 
    'LAVE-VAISSELLE & ACCESSOIRES')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Lave-Vaisselle'), 
    'Lave-Vaisselle')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Celliers  Refroidisseurs'), 
    'CELLIERS & REFROIDISSEURS')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Celliers  Refroidisseurs  vin'), 
    'Celliers & Refroidisseurs a vin')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Sofas  Divans  des Prix Imbattables  Brault  Martineau (2)/a_Refroidisseurs  eau'), 
    'Refroidisseurs d\'eau')

WebUI.click(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/span_Matelas'))

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/a_Matelas  Sommiers'), 
    'MATELAS & SOMMIERS')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/a_Ensembles de matelas  sommiers'), 
    'Ensembles de matelas & sommiers')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/a_Plateformes'), 
    'Plateformes')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/a_Lits'), 
    'LITS')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/a_Literie'), 
    'LITERIE')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/a_Literie  Jets'), 
    'Literie & Jetés')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/a_Protge-matelas'), 
    'Protége-matelas')

WebUI.click(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration (1)/span_Dcoration'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration (1)/a_Tapis et rideaux'), 
    'TAPIS ET RIDEAUX')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration (1)/a_Tapis'), 
    'Tapis')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration (1)/a_Coussins et jets'), 
    'COUSSINS ET JETÉS')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration (1)/a_Coussins dcoratifs'), 
    'Coussins décoratifs')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration (1)/a_Jets'), 
    'Jetés')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration (1)/a_Objets dcoratifs'), 
    'OBJETS DÉCORATIFS')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration (1)/a_Miroirs  Tableaux'), 
    'Miroirs & Tableaux')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration (1)/a_Rangement'), 
    'Rangement')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration (1)/a_Luminaires'), 
    'LUMINAIRES')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration (1)/a_Lampes de table'), 
    'Lampes de table')

WebUI.verifyElementText(findTestObject('Brault/Brault_Desktop/ValidationGlobalMenu/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration (1)/a_Lampes sur pied'), 
    'Lampes sur pied')

WebUI.closeBrowser()

