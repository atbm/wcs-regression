import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.UrlBrault)

WebUI.sendKeys(findTestObject('InfLettre/Page_Brault  Martineau  Meubles lectromnagers lectronique  Dcoration/input_Abonnez-vous  notre infolettre_email'), 
    'martineau@martineau.com')

WebUI.click(findTestObject('Brault/Brault_Desktop/InfosLettre/span_Abonnez-vous  notre infolettre_svg submitImage'))

title = WebUI.getWindowTitle()

WebUI.delay(5)

WebUI.verifyMatch(title, 'Inscription  l\'infolettre', false)

WebUI.sendKeys(findTestObject('InfLettre/Page_Inscription  linfolettre/input__firstName'), 'Teresa')

WebUI.sendKeys(findTestObject('InfLettre/Page_Inscription  linfolettre/input__lastName'), 'Martineau')

WebUI.clearText(findTestObject('InfLettre/Page_Inscription  linfolettre/input__email'), FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('InfLettre/Page_Inscription  linfolettre/input__email'), 'martineau@martineau.com')

WebUI.sendKeys(findTestObject('InfLettre/Page_Inscription  linfolettre/input__emailConfirmation'), 'martineau@martineau.com')

WebUI.click(findTestObject('Brault/Brault_Desktop/InfosLettre/info'))

WebUI.click(findTestObject('Brault/Brault_Desktop/InfosLettre/div_Confirmer mon abonnement'))

WebUI.closeBrowser()

