import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.null)

WebUI.deleteAllCookies()

WebUI.maximizeWindow()

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/SocialMedia/a_Restez en contact _footerFacebookLink'))

WebUI.switchToWindowIndex('1')

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Brault & Martineau - Accueil | Facebook', false)

WebUI.delay(3)

WebUI.switchToWindowIndex(0)

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/SocialMedia/a_Restez en contact _instagramLink'))

WebUI.switchToWindowIndex('2')

WebUI.delay(3)

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Brault & Martineau (@braultetmartineau) • Photos et vidéos Instagram', false)

WebUI.delay(3)

WebUI.switchToWindowIndex(0)

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/SocialMedia/a_Restez en contact _pinterestLink'))

WebUI.switchToWindowIndex('3')

WebUI.delay(3)

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Brault et Martineau (braultetmartineau) sur Pinterest', false)

WebUI.delay(3)

WebUI.switchToWindowIndex(0)

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/SocialMedia/a_Restez en contact _youtubeLink'))

WebUI.delay(3)

WebUI.switchToWindowIndex('4')

WebUI.delay(3)

title = WebUI.getWindowTitle()

WebUI.delay(3)

WebUI.verifyMatch(title, 'Brault & Martineau - YouTube', false)

WebUI.delay(3)

WebUI.switchToWindowIndex(0)

WebUI.closeBrowser()

