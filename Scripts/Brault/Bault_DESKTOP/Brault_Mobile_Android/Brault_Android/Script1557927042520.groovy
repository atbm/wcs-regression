import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('C:\\Users\\sancpaul\\Desktop\\QA test\\androidapp\\APIDemos.apk', true)

Mobile.pressBack()

WebUI.delay(5)

Mobile.tap(findTestObject('Brault_phone/android.widget.TextView0 - Brault'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault_Mobile/android.view.View8 (1)'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault_Mobile/android.view.MenuItem7 - Se connecter  Sinscrire'), 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault_phone/android.view.View55 - Crer un compte'), 0, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

Mobile.setText(findTestObject('Brault_Mobile/android.widget.EditText1'), 'BraultMartineau123', 0)

WebUI.delay(5)

Mobile.setText(findTestObject('Brault_Mobile/android.widget.EditText2'), 'BraultMartineau123', 0)

WebUI.delay(5)

Mobile.tap(findTestObject('Brault_Mobile/android.widget.EditText3'), 'Martineau', 0)

WebUI.delay(5)

Mobile.setText(findTestObject('Brault_Mobile/android.widget.EditText3'), 'Martineau', 0)

WebUI.delay(5)

Mobile.setText(findTestObject('Brault_Mobile/android.widget.EditText4'), 'Brault', 0)

WebUI.delay(5)

Mobile.setText(findTestObject('Brault_Mobile/android.widget.EditText0'), 'brautl@brautl.brault', 0)

