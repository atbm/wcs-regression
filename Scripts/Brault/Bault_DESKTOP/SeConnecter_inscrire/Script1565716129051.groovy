import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.UrlBrault)

WebUI.maximizeWindow()

WebUI.delay(3)

WebUI.click(findTestObject('Brault/Brault_Desktop/SeConnecter_inscrire/a_Se connecter  Sinscrire'))

WebUI.click(findTestObject('Brault/Brault_Desktop/SeConnecter_inscrire/creerCompte'))

mail = CustomKeywords.'var.addNewMail.randomMail'()

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_inscrire/input__logonId'), mail)

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_inscrire/input__logonPassword'), 'Economax123')

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_inscrire/input__logonPasswordVerify'), 'Economax123')

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_inscrire/input__firstName'), 'testerQA')

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_inscrire/input__lastName'), 'testerQA')

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_inscrire/input__phone1'), '5142591233')

WebUI.click(findTestObject('Brault/Brault_Desktop/SeConnecter_inscrire/div_Franais'))

WebUI.click(findTestObject('Brault/Brault_Desktop/SeConnecter_inscrire/td_Franais'))

WebUI.delay(3)

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/LaPoste/adresse'), '8600 place marien')

WebUI.delay(4)

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/LaPoste/adresse'), 'Montréal-Est QC H1B 5W8')

WebUI.delay(4)

WebUI.doubleClick(findTestObject('Brault/Brault_Desktop/SeConnecter_inscrire/poste'))

WebUI.delay(4)

WebUI.click(findTestObject('Brault/Brault_Desktop/SeConnecter_inscrire/conect'))

