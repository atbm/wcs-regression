import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.UrlBrault)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Brault/Brault_Desktop/SeConnecter_inscrire/a_Se connecter  Sinscrire'))

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/input_Identifiant (adresse courriel) _logonId'), 'testwebbm+00002@gmail.com')

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/input_Mot de passe _logonPassword'), 'test123*')

WebUI.click(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/span_Se connecter'))

WebUI.click(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/a_Modifier ladresse'))

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/Page_Informations personnelles/input_Mot de passe (min 6 caractres dont au moins un chiffre et une lettre) zone_logonPassword_old'), 
    'test123*')

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/Page_Informations personnelles/input_Vrifier le mot de passe zone_logonPasswordVerify_old'), 
    'test123*')

WebUI.click(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/Page_Informations personnelles/a_Modifier'))

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/Page_Informations personnelles/input_Mot de passe (min 6 caractres dont au moins un chiffre et une lettre) zone_logonPassword_old'), 
    'test123*')

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/Page_Informations personnelles/input_Vrifier le mot de passe zone_logonPasswordVerify_old'), 
    'test123*')

WebUI.clearText(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/Page_Informations personnelles/input__address1'), FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/Page_Informations personnelles/input__address1'), '8600 place marien')

WebUI.verifyElementPresent(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/Page_Informations personnelles/span__address1_validate'), 
    0)

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/Page_Informations personnelles/input_Appartement_address2'), 
    '2')

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/Page_Informations personnelles/input__city'), 'Montreal-Est')

WebUI.clearText(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/Page_Informations personnelles/input__city'), FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/Page_Informations personnelles/input__zipCode'), 'H1B 5W8')

WebUI.verifyElementPresent(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/Page_Informations personnelles/span__zipCode_validate'), 
    0)

WebUI.click(findTestObject('Brault/Brault_Desktop/SeConnecter_Register/Page_Informations personnelles/div_Modifier les renseignements personnels'))

