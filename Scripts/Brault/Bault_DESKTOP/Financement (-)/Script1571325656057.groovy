import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Finance/Page_Livraison - Brault  Martineau/span_Poursuivre vers la facturation'))

WebUI.click(findTestObject('Finance/Page_Facturation - Brault  Martineau/input_Payer par carte de crdit_payment_method'))

WebUI.click(findTestObject('Finance/Page_Facturation - Brault  Martineau/label_En cochant'))

WebUI.setText(findTestObject('Finance/Page_Facturation - Brault  Martineau/input_Numro de carte_finance_card'), '4530910000012345')

WebUI.setText(findTestObject('Finance/Page_Facturation - Brault  Martineau/input_concat(Qu  est-ce qu  un code de scurit)_financing_card_cvv'), 
    '325')

WebUI.click(findTestObject('Finance/Page_Facturation - Brault  Martineau/div_Suivant'))

